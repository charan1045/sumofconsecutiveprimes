#include<bits/stdc++.h>
#define IOS ios::sync_with_stdio(false);cin.tie(0);
using namespace std;
typedef long long ll;
const ll INF=1e6;
ll binpow(ll a,ll b,ll mod){  // miller rabin primality test 
	ll res=1;
	while(b>0){
		if(b%2)res=(res*a)%mod;
		a=(a*a)%mod;
		b>>=1;}
		return res%mod;}
bool check_composite(ll a,ll d,ll s,ll n){
	ll x=binpow(a,d,n);
	if(x==1||x==n-1)return false;
	for(ll r=1;r<s;r++){
		x=(x*x)%n;
		if(x==n-1)return false;}
		return true;}
bool millerrabin(ll p){
	if(p<4)return p==2||p==3;
	ll d=p-1;
	ll s=0;
	while((d&1)==0){
		d>>=1;
		s++;}
	 for(int i=0;i<5;i++){
		 ll a=(2+rand()%(p-3));
		 if(check_composite(a,d,s,p))return false;}
		 return true;}
int main(){
	IOS;
	ll n;
	cin>>n;
	vector<int>primes(INF+1,0);
	vector<bool>l(INF+1,true);
	primes[0]=2;
	int cnt=1;
	for(ll i=3;i<INF;i+=2){
		if(l[i]){
			for(ll j=2*i;j<INF;j+=i){
				l[j]=false;}
				primes[cnt++]=i;}}
			//	for(int i=0;i<n;i++)cout<<primes[i]<<" ";
	ll sum=0;
	ll i=0;
	ll count=0;
	while(sum<n){
		if(sum>INF&&sum%2){
			if(millerrabin(sum))count++;}
			else{
				if(sum%2 && l[sum]){
					count++;}}
				sum+=primes[i];
				i++;}
				cout<<count<<endl;}
